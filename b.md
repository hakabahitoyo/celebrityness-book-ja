# 付録B: マストドンのユーザー発見メソッドの親セレブリティ性の定量的な評価の実験

この本では、ユーザー発見メソッドの親セレブリティ性の定量的な評価、すなわち親セレブリティ性スコアは、以下のように定義される: ユーザー発見メソッドが推挙するユーザーのフォロワー数の幾何平均。この章では、マストドンと他の脱中央集権なソーシャルネットワークのユーザー発見メソッドの、親セレブリティ性スコアを測定する。

下記の表では、ユーザー発見メソッドとその親セレブリティ性スコアが示される。AからKまでのラベルは、この本の[第2章](02.md)と同じである。

この実験では、日本時間の2018年8月13日の20時から21時までの間に、ユーザー発見メソッドを観測した。パーソナライズされたユーザー発見メソッドのためには、vaginaplant<span>@</span>3.distsn.org [2] アカウントを使用した。例外として、Pawooの「おすすめユーザー」パネルを観測するために、hakabahitoyo<span>@</span>pawoo.net [3] アカウントを使用した。検索エンジンに対しては、キーワードは「ハッピーシュガーライフ」[4] を使用した。フォロワー数は、同じ日の21時から23時までの間に観測した。

親セレブリティ性スコアは、ユーザー発見メソッドが推挙するユーザーのフォロワー数の幾何平均である。ユーザー発見メソッドが推挙するユーザー数が20よりも多い場合は、先頭の20ユーザーについて計算した。推挙されるユーザーが20未満の場合は、すべてのユーザーを計算に入れた。フォロワー数が0であるユーザーについては、フォロワー数が1であるものとみなして計算した。なぜなら、0は幾何平均を破壊するからである。

<table>
<tr>
    <th>ラベル</th>
    <th>ユーザー発見メソッド</th>
    <th>ウェブサイト</th>
    <th>親セレブリティ性スコア</th>
</tr>
<tr>
    <td>A</td>
    <td>Mastodonランキング (フォロワー数)</td>
    <td>[mastodon.userlocal.jp](http://mastodon.userlocal.jp/) (停滞)</td>
    <td>1941.5</td>
<tr>
<tr>
    <td>B</td>
    <td>おすすめフォロワー</td>
    <td>[followlink.osa-p.net/recommend.html](https://followlink.osa-p.net/recommend.html) (停滞)</td>
    <td>1312.7</td>
<tr>
<tr>
    <td>C</td>
    <td>マストドン (Mastodon) ユーザなら必ずフォローしたい！ アカウント一覧 - 運営・有名人・メディアほか</td>
    <td>[takulog.info/mastodon-famous-accounts](https://takulog.info/mastodon-famous-accounts/)</td>
    <td>1178.3</td>
<tr>
<tr>
    <td>D</td>
    <td>Mastodonランキング (ブースト数)</td>
    <td>[mastodon.userlocal.jp/?fqdn=boost](http://mastodon.userlocal.jp/?fqdn=boost) (停滞)</td>
    <td>1123.7</td>
<tr>
<tr>
    <td>E</td>
    <td>Pawoo</td>
    <td>[pawoo.net](https://pawoo.net/)</td>
    <td>558.3</td>
<tr>
<tr>
    <td>F</td>
    <tdホームタイムライン</td>
    <td>公式</td>
    <td>398.3</td>
<tr>
<tr>
    <td>G</td>
    <td>連合タイムライン</td>
    <td>公式</td>
    <td>404.9</td>
<tr>
<tr>
    <td>H</td>
    <td>ローカルタイムライン</td>
    <td>公式</td>
    <td>79.8</td>
<tr>
<tr>
    <td>I</td>
    <td>tootsearch</td>
    <td>[tootsearch.chotto.moe](https://tootsearch.chotto.moe/)</td>
    <td>134.1</td>
<tr>
<tr>
    <td>J</td>
    <td>User Matching for Fediverse</td>
    <td>[distsn.org/user-match.html](https://distsn.org/user-match.html) (リンク切れ)</td>
    <td>39.6</td>
<tr>
<tr>
    <td>K</td>
    <td>Newcomers in Fediverse</td>
    <td>[distsn.org/user-new.html](https://distsn.org/user-new.html) (リンク切れ)</td>
    <td>1.8</td>
<tr>
</table>

## References

[1] 墓場人夜, ユーザーディスカバリーメソッドのフェアネスの定量的評価手法の提案,\
https://gitlab.com/distsn-documents/fairness-quantity (リンク切れ)


[2] https://3.distsn.org/vaginaplant (リンク切れ)


[3] https://pawoo.net/@hakabahitoyo


[4] 鍵空とみやき, ハッピーシュガーライフ,\
https://happysugarlife.tv (リンク切れ)

